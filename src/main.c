#include "mem.h" 
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
 

bool test_normal_allocation();
bool test_free_single_block();
bool test_free_multiple_blocks();
bool test_expand_existing_region();
bool test_allocate_new_region();

int main() {
    printf("Running memory allocator tests...\n");

    int tests_passed = 0;
    tests_passed += test_normal_allocation();
    tests_passed += test_free_single_block();
    tests_passed += test_free_multiple_blocks();
    tests_passed += test_expand_existing_region();
    tests_passed += test_allocate_new_region();

    printf("Total tests passed: %d/%d\n", tests_passed, 5);
    return 0;
}

bool test_normal_allocation() {
    void* block = _malloc(100);
    if (block == NULL) {
        printf("Test normal allocation: Failed\n");
        return false;
    }
    _free(block);
    printf("Test normal allocation: Passed\n");
    return true;
}

bool test_free_single_block() {
    void* block1 = _malloc(100);
    void* block2 = _malloc(200);
    _free(block1);
    if (block2 == NULL) {
        printf("Test free single block: Failed\n");
        return false;
    }
    _free(block2);
    printf("Test free single block: Passed\n");
    return true;
}

bool test_free_multiple_blocks() {
    void* block1 = _malloc(100);
    void* block2 = _malloc(200);
    _free(block1);
    _free(block2);
    printf("Test free multiple blocks: Passed\n");
    return true;
}

bool test_expand_existing_region() {
    void* block1 = _malloc(100);
    void* block2 = _malloc(1024 * 1024); 
    if (block2 == NULL) {
        _free(block1);
        printf("Test expand existing region: Failed\n");
        return false;
    }
    _free(block1);
    _free(block2);
    printf("Test expand existing region: Passed\n");
    return true;
}

bool test_allocate_new_region() {
    void* block1 = _malloc(1024 * 1024); 
    void* block2 = _malloc(1024 * 1024); 
    if (block2 == NULL) {
        _free(block1);
        printf("Test allocate new region: Failed\n");
        return false;
    }
    _free(block1);
    _free(block2);
    printf("Test allocate new region: Passed\n");
    return true;
}
